# Example HTML, JavaScript Project

This project is based on a simple HTML page and uses the following libraries and services:

* [jQuery](http://api.jquery.com/)
* [D3.js](http://d3js.org/)
	* [Example D3 Visualisations](http://christopheviau.com/d3list/)
	* [API Reference](https://github.com/mbostock/d3/wiki/API-Reference)
* [Google Image Search (Deprecated but still works)](https://developers.google.com/image-search/v1/devguide)
* [Bootstrap (CSS library)](http://getbootstrap.com/)

## The Data Set

The Titanic data set is used and it is hosted at: 

* [http://kdeg-vm-44.scss.tcd.ie/content/hackathon/](http://kdeg-vm-44.scss.tcd.ie/content/hackathon/)