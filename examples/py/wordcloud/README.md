This is a simple script that takes a text file and returns a png of the words in the text file ordered by their frequency. It highlights words from a particular set, and removes stopwords

It's a handy example for basic text analysis and visualisation libraries in python, and uses some straightforward code. Feel free to hack away.

Handy nltk resources <http://nltk.org/book/> <http://nltk.org/howto/>
Handy book on PIL <http://effbot.org/imagingbook/image.htm>

