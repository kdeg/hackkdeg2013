#!/usr/bin/env python

# -*- coding: utf-8 -*-

""" This program takes a text file and returns a word cloud. It requires nltk and pil or pillow. """
from __future__ import division

import os, sys 
# http://www.python.org/dev/peps/pep-0238/
#import the toolkit and regular expressions
import nltk, re
from nltk.tokenize import word_tokenize, sent_tokenize
#handle utf-8 files
import codecs
#handy structure for counting
from collections import Counter, defaultdict
#imaging
from PIL import Image, ImageFont, ImageDraw

__author__ = "Alex O'Connor"
__copyright__ = "Copyright 2012, Alex O'Connor"
__credits__ = ["Alex O'Connor"]
__license__ = "Copyright"
__version__ = "0.1"
__email__ = "<Alex.OConnor@scss.tcd.ie>"
__status__ = "Prototype"

def make_cloud(text):
    counts = get_counts(text)
    make_image(counts)

def make_image(counts):
    img_size = (1000, 2000)
    img = Image.new("RGB", img_size, (255,255,255))
    draw = ImageDraw.Draw(img)
    font_size = 12 
    space_size = 4 

    #invert the collection; create a list of words by count
    sizes = defaultdict(list)
    for word, count in counts.most_common():
        sizes[count].append(word)

    #do the layout
    # this is the wrong approach. a smarter way would be to start at the middle
    # with the biggest word, work out its bounding box, then place words as near
    # to the center, advancing them if they hit each other
    offset = [space_size,space_size]
    for i, wordcount in enumerate(sizes):
        font = ImageFont.truetype("DroidSansMono.ttf", font_size)
        for j, word in enumerate(sizes[wordcount]):
            #calculate the size of the text
            text_size = draw.textsize(word, font=font)
            #check for new line needed
            if(offset[0] + text_size[0] > img_size[0]):
                offset[1] += text_size[1] + space_size
                offset[0] = space_size 
            #make marked words relating to gender 
            highlights = ['man', 'woman', 'male', 'female', 'men', 'women', 'children']
            colour = (0,0,200)
            if (word in highlights):
                colour = (255, 0, 0)
            draw.text(offset, word, colour, font=font)
            offset[0] += text_size[0] + space_size


    img.save('text.png', 'PNG')

def get_counts(text):
    # split it into tokens
    tokens = [word for sent in sent_tokenize(text) for word in word_tokenize(sent)]
    # filter the tokens and count them
    # [^\W\d_] as a negated character class allows any alphanumeric character except for digits and underscore. http://stackoverflow.com/questions/5717886/python-regex-extracting-whole-words
    filter_expr = re.compile('[^\W\d_]', re.IGNORECASE)
    words = [word.lower() for word in tokens if re.match(filter_expr, word)]
    #remove stop-words (this is the default lucene stop-word list for English)
    stop_words = ['a', 'an', 'and', 'are', 'as', 'at', 'be', 'but', 'by','for', 'if', 'in', 'into', 'is', 'it','no', 'not', 'of', 'on', 'or', 'such','that', 'the', 'their', 'then', 'there', 'these','they', 'this', 'to', 'was', 'will', 'with']
    words = [word for word in words if word not in stop_words]
    # use a list comprehension to filter the tokens, lowercase them and then push them into the counter
    counts = Counter(words)
    return counts

def parse_text_file(textfile):
    with codecs.open(textfile, encoding='utf-8') as text_file:
        return make_cloud(text_file.read())

if __name__ == '__main__':
#should probably use smarter things like docopt, but for simple arguments
    try:
        print parse_text_file(sys.argv[1])
    except IndexError as e:
        print "please provide the filename as a single argument"
