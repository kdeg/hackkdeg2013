Some possible libraries, basic tools

* Play framework <http://playframework.com>
* D3.js <http://d3js.org/>
* CKAN <http://ckan.org/>
* mysociety <https://secure.mysociety.org/cvstrac/dir?d=mysociety/twfy>
* apache drill <http://incubator.apache.org/drill/>
* weka <http://www.cs.waikato.ac.nz/ml/weka/>
* mahout <http://mahout.apache.org/>
* ipython notebook <http://ipython.org/notebook.html>
* tika <http://tika.apache.org>
* stanford nlp group resources <http://www-nlp.stanford.edu/links/statnlp.html>
* Elastic Search <http://www.elasticsearch.org/>
* nltk <http://nltk.org/>
* Trilby <http://kwijibo.github.io/trilby/>
* Linked Data Tools <http://linkeddata.org/tools>
