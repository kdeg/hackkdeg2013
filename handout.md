#KDEG Winter Hackathon 2013&mdash; Team Worksheet

##The goal: work together to gather some data, build and use some tools to build a story from that data, tell the tale to the group.

#Team Name:
*come up with a name and a logo*

#Team Members:
*name (role)* 

1. &nbsp;
1. &nbsp;
1. &nbsp;

#Team Keywords

##1.
##2.


*you'll draw keywords to work on from the 'hat'*

#Data List
*where are you going to gather your data?*

1. &nbsp;
1. &nbsp;
1. &nbsp;

#Feature List
*what's your tool going to do?*

1. &nbsp;
1. &nbsp;
1. &nbsp;

#The story
*what's the insight, in three bullet points?*

1. &nbsp;
1. &nbsp;
1. &nbsp;