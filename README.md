# Hacking KDEG, 2013

This is the basic information for the KDEG Hackathon event at the Winter Workshop 2013. The idea of this is to allow people to form groups and to get some code down to build the smallest viable tool in the shortest period. 

There are (almost) no rules, but some suggestions and resources are included below.

## The Goal

The idea is to form a team, come up with an idea and implement the simplest, demoable form of that idea, and communicate its value to the rest of the group. With that in mind, the idea would ideally be based on some of the research areas / interests / technologies in KDEG. However, given the _extremely_ short timespan, managing your ambition is key.

Some suggestions:

* use a public or open dataset
* use libraries that are well-documented
* focus on one part: visualiation, data processing, novel application, user interface 

Some suggested resources and libraries are included in the files accompanying this project.

## The (few) Rules

1. You should aim to deliver _demo-able_ code
1. You should work as a team
1. You should have fun
1. You should stick to the time-frame

## The Time-frame

This is a four-hour hack session, which makes it approximately 10% of what a normal event would allocate. With that in mind, we have a radically reduced the time-line and ambition.

* 13:30 (over lunch) Team list will be posted, chance to discuss ideas and come up with how the project will be approached
* 14:00 Initial pitches -- each team has 90seconds to pitch their idea
* 14:00 Furious coding begins
* 16:00 half-way update: Any demo-able code is shown, each team reports progress (max 3mins each)
* 17:00 Final Demo: Each team demo's their system, vote cards are distributed
* 17:30 Wrap up and commit -- all code goes into a repo, and votes are collected

### Notes
* Presentations happen while work can continue.

## The Prize
At the end of the session, there will be a vote by the jury, and a people's choice award. Each team member gets one vote for the people's choice, and the panel of Academics will decide which project they like most. A prize will be awarded at the social event later on. 

