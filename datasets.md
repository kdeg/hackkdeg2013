Some possible sources of data:

* Amazon Public Data Sets <http://aws.amazon.com/publicdatasets>
* Europeana <http://pro.europeana.eu/datasets>
* Fingal Open Data <http://data.fingal.ie/>
* Property Price Register (Ireland) <ps://www.propertypriceregister.ie/website/npsra/pprweb.nsf/PPRDownloads?OpenForm>
* Oireachtas Debates <http://oireachtasdebates.oireachtas.ie/>
* Irish Central Statistics Office <http://www.cso.ie/en/index.html>
* Eurostat <http://epp.eurostat.ec.europa.eu/portal/page/portal/statistics/search_database>

